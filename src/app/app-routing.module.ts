import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './login/login.component'
import { CreateuserComponent } from './createuser/createuser.component';
import { AuthGuard } from './auth.guard';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
// import { LoginGuard } from './login.guard';
// import { CreateuserlistComponent } from './createuserlist/createuserlist.component';


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'resepassword',
    component: ResetpasswordComponent,
  },
  {
    path: 'createuser',
    component: CreateuserComponent, canActivate: [AuthGuard]
  },
  // {
  //   path: 'createduserlist',
  //   component: CreateuserlistComponent,
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
