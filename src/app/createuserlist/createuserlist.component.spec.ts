import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateuserlistComponent } from './createuserlist.component';

describe('CreateuserlistComponent', () => {
  let component: CreateuserlistComponent;
  let fixture: ComponentFixture<CreateuserlistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateuserlistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateuserlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
