import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import { JwtService } from './jwt.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private JwtService: JwtService , private router: Router){ }
  canActivate(): boolean {
    if(this.JwtService.getToken()){
     return true;
    }
 
    else{
      this.router.navigate(['/login'])
      return false;
    }
  }
  
}
