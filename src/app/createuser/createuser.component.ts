import { Component, EventEmitter, Input, OnInit, Output,ViewChild } from '@angular/core';
import { FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../common.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-createuser',
  templateUrl: './createuser.component.html',
})
export class CreateuserComponent implements OnInit {
  personalDetailForm: any;
  customerList:any = [];
  partnerCode:any;
  constructor(private toastr: ToastrService , private common : CommonService ,private fb: FormBuilder,  private router: Router,) { }

  ngOnInit(): void {
    this.personalDetailForm = this.fb.group({
      fullName: ['', [Validators.required , Validators.pattern(/[!^\w\s]$/)]],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required, Validators.maxLength(15), Validators.pattern('[1-9]{1}[0-9]{9}')]],
    });

    this.listofuser();
    
    this.partnerCode = localStorage.getItem("partner_code");

  }

  listofuser(){
      this.common.userList().subscribe(
        success => {
          this.customerList = success.result.rm;
        this.customerList.reverse();
         console.log(this.customerList.reverse());
        },
        error=>{
          console.log("message");
          console.log(error);
          this.toastr.error(error.err.message, "", { timeOut: 3000, disableTimeOut: false, });
        }
  
      );
  }

  signUp(){
    let data = {
      rm_name : this.personalDetailForm.get('fullName').value,
      rm_mobile_no : this.personalDetailForm.get('phoneNumber').value.toString(),
      rm_email_id : this.personalDetailForm.get('email').value,
    }

    this.common.CreateUser(data).subscribe(
      success => {
        debugger;  
      if(success.status == 1){
        this.toastr.success(success.message, "", { timeOut: 3000, disableTimeOut: false, });
        this.personalDetailForm.reset();
        this.listofuser();
        
      }
      else if(success.status == 0){
        this.toastr.error(success.message, "", { timeOut: 3000, disableTimeOut: false, });
      }
      },
      error=>{
        this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });
      }
    );
 }

}
