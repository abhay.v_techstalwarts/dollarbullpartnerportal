import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../common.service';
import { JwtService } from '../jwt.service';
import { ToastrService } from 'ngx-toastr';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
  rmForm: any;
  rmForgotPasswordForm:any;
  constructor(private toastr: ToastrService ,private modalService: NgbModal, config: NgbModalConfig ,private jwtService: JwtService, private common: CommonService ,private fb: FormBuilder,private router: Router) { }

  ngOnInit(): void {
    this.rmForm = this.fb.group({
      email_id: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required,]],
    });

    this.rmForgotPasswordForm = this.fb.group({
      email_id: ['', [Validators.required, Validators.email]],
    });
  }

  rm_login() {
    if (this.rmForm) {
      let data = {
        email_id: this.rmForm.get('email_id').value,
        password: this.rmForm.get('password').value,
      }
      this.common.Login(this.rmForm.value).subscribe(
        success => {  
        if(success.status == 1){
          this.jwtService.save_Mobile_number(success.result.mobile_no);
          this.jwtService.save_Email_ID(success.result.email_id);
          this.jwtService.savePartnercode(success.result.partner_code);
          this.jwtService.saveToken(success.result.token);
          this.jwtService.save_Partner_Id(success.result.partner_id);
          this.toastr.success(success.message, "", { timeOut: 3000, disableTimeOut: false, });

          this.router.navigate(['/createuser'], { replaceUrl: true });
        }
        },
        error=>{
          console.log("message");
          console.log(error);
          this.toastr.error(error.error.message, "", { timeOut: 3000, disableTimeOut: false, });

        }
  
      )
      // this.signUpService.rm_login(data).subscribe((res) => {
      //   let response: any = res;
      //   this.jwtService.saveToken(response.result.token);
      //   this.jwtService.savePartnercode(response.result.partner_code);
      //   this.jwtService.saveRM_AdvisorCode(response.result.rm_advisor_code);
      //   this.jwtService.saveRM_Email_ID(response.result.rm_email_id);
      //   this.jwtService.saveRM_Master_Id(response.result.rm_master_id);
      //   this.jwtService.saveRM_Mobile_number(response.result.rm_mobile_no);
      //   this.jwtService.saveRM_Referal_Code(response.result.rm_referal_code);
      //   this.showLoader = false;
      //   this.router.navigate(['/dashboard'], { replaceUrl: true });
      //   // console.log("rm_login", res);
      //   // console.log("rm_login", JSON.stringify(res));
      // },
      // (err) => {
      //   this.showLoader = false;
      //   console.log(err)
      //   this.otp_screen_msg = err.error.message;
      // }
      // );


    }
  }

  submitForgot(){
    localStorage.setItem('email_id', this.rmForgotPasswordForm.get('email_id').value);
    let data = {
      email_id : this.rmForgotPasswordForm.get('email_id').value,
    }
    this.common.forgotPasswordLink(data).subscribe((res) => {
      //@ts-ignore
      console.log(res.message);
      //@ts-ignore
        this.toastr.success(res.message, "", { timeOut: 3000, disableTimeOut: false, });
        this.rmForgotPasswordForm.reset();
        this.closeModal();
    },
    (err) => {
      // this.showLoader = false;
      this.toastr.error(err.error.message, "", { timeOut: 3000, disableTimeOut: false, });
    })
  }
  closeModal(){
    this.rmForgotPasswordForm.reset();
    this.modalService.dismissAll();
  }
  openForgotPasswordModal(forgotPasswordModal:any) {
    this.modalService.open(forgotPasswordModal);
  }

}
