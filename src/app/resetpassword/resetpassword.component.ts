import { CommonService } from './../common.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormsModule, Validators } from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {
  //@ts-ignore
  password:number;
  //@ts-ignore
  confirmPassword:number;
  //@ts-ignore
  errorConfirmPassword:boolean;
  //@ts-ignore
  errorPassword:boolean;
  resetButtonDisabled:boolean=true;
  resetPasswordForm:any;
  reset_code:any;
  constructor(private router: Router, private activatedRoute: ActivatedRoute , private toastr: ToastrService, private commonService: CommonService,private fb: FormBuilder,) { }

  ngOnInit(): void {
    
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.reset_code = params.reset_code;
    });

    
    this.resetPasswordForm = this.fb.group({
      password: ['', [Validators.required ,Validators.min(6)]],
      confirmpassword: ['', [Validators.required ,Validators.min(6)]],
    });
  }

  passwordPress(event:any) {
    this.password = event.target.value;
    console.log(this.password);
  }
  confirmPasswordPress(event:any){
    this.confirmPassword = event.target.value;
    if( this.confirmPassword != this.password){
      this.resetButtonDisabled = true;
      this.errorConfirmPassword = true;
    }
    else{
      this.resetButtonDisabled = false;
      this.errorConfirmPassword = false;
    }
  }

  submitResetPassword(){    
    // let email_id = localStorage.getItem('email_id');
     let data = {
       reset_code : this.reset_code,
       password : this.resetPasswordForm.get('confirmpassword').value,
      //  email_id : email_id,
     }
     this.commonService.resetPassword(data).subscribe((res:any) => {
       //@ts-ignore

       this.toastr.success(res.message, "", { timeOut: 3000, disableTimeOut: false, });
       if(res.status == 1){
       this.commonService.logout().subscribe((res:any) => {
        this.router.navigate(['/login']);
      });
    }
     },
     (err) => {
       this.toastr.error(err.error.message, "", { timeOut: 3000, disableTimeOut: false, });
     })
   }

}
