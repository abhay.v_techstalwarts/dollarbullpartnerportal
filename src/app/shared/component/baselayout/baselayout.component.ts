import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/common.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-baselayout',
  templateUrl: './baselayout.component.html',
})
export class BaselayoutComponent implements OnInit {

  constructor(private toastr: ToastrService , private common : CommonService , private router: Router) { }

  ngOnInit(): void {
  }

  logout(){
    this.common.logout().subscribe((res) => {
      this.toastr.success("logout successful", "", { timeOut: 3000, disableTimeOut: false, });
      this.router.navigate(['/login']);
    });
  }

}
