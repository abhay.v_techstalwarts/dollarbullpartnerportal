import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse,HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from './../environments/environment';
import { JwtService } from './jwt.service';
@Injectable({
  providedIn: 'root'
})
export class CommonService {
  apiUrl: string = environment.apiUrl;
  constructor(private http: HttpClient , private jwtService: JwtService,) { }
  Login(data: any): Observable<any> {
    return this.http.post(this.apiUrl+'/login',data)
  }
  CreateUser(data: any): Observable<any> {
    const headers = this.getHeaders();
    return this.http.post(this.apiUrl+'/createrm',data,{headers})
  }
  userList(): Observable<any> {
    const headers = this.getHeaders();
    return this.http.get(this.apiUrl+'/getrm',{headers});

  }

  public forgotPasswordLink(data:any){
    // const headers = this.getHeaders();
    return this.http.post(this.apiUrl+'/forgotpassword', data);
  }

  
  public resetPassword(data:any){
    return this.http.post(this.apiUrl+'/resetpassword', data);
  }

  public logout(): Observable<any> {
    this.jwtService.destroyToken();
    this.jwtService.destroyPartnercode();
    this.jwtService.destroyEmailid();
    this.jwtService.destroyMobilenumber();
    this.jwtService.destroyPartnerId();
    return of(
      new HttpResponse({
        status: 1,
      })
    );
  }

  public getHeaders(){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
       'Access-Control-Allow-Origin' : '*',
       Authorization: `Bearer ${this.jwtService.getToken()}`,
    });
    return headers;
  }
}
