import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaselayoutComponent } from './component/baselayout/baselayout.component';



@NgModule({
  declarations: [
    BaselayoutComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    BaselayoutComponent
  ]
})
export class SharedModule { }
