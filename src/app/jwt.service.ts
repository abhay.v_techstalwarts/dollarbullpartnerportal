import { Injectable } from '@angular/core';
// import jwtDecode from 'jwt-decode';

@Injectable({
  providedIn: 'root',
})
export class JwtService {
  getToken(): string {
    return window.localStorage['jwtToken'];
  }

  saveToken(token: string): void {
    window.localStorage['jwtToken'] = token;
  }

  destroyToken(): void {
    window.localStorage.removeItem('jwtToken');
  }

  // rm portal
  
  savePartnercode(partner_code:string):void{
    window.localStorage['partner_code'] = partner_code;
  }

  getPartnercode():void{
    return window.localStorage['partner_code'];
  }

  destroyPartnercode():void{
    window.localStorage.removeItem('partner_code');
  }

  save_Email_ID(email_id:string):void{
    window.localStorage['email_id'] = email_id;
  }

  get_Email_ID():void{
    return window.localStorage['email_id'];
  }

  destroyEmailid():void{
    window.localStorage.removeItem('email_id');
  }

  save_Partner_Id(partner_id:string):void{
    window.localStorage['partner_id'] = partner_id;
  }

  get_Partner_Id():void{
    return window.localStorage['partner_id'];
  }

  destroyPartnerId():void{
    window.localStorage.removeItem('partner_id');
  }

  save_Mobile_number(mobile_no:string):void{
    window.localStorage['mobile_no'] = mobile_no;
  }

  get_Mobile_number():void{
    return window.localStorage['mobile_no'];
  }
  destroyMobilenumber():void{
    window.localStorage.removeItem('mobile_no');
  }



  // end rm portal

  // readTokenData(): any {
  //   const token = this.getToken();
  //   const payload = jwtDecode(token);
  //   return payload;
  // }
}
